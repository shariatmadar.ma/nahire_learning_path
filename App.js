import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from 'react-apollo';

import Navigator from './src/Navigator';

const client = new ApolloClient({
  uri: 'http://5.9.198.230:4000/graphql'
})

const App = () => {
  return (
    <ApolloProvider client={client}>
      <NavigationContainer>
        <Navigator />
      </NavigationContainer>
    </ApolloProvider>
  );
};

export default App;
