import React from 'react';

import { createStackNavigator } from '@react-navigation/stack'

import Starting from './screens/Starting';
import Destination from './screens/Destination';
import CarSelection from './screens/CarSelection';
import WaitingForCar from './screens/WaitingForCar';
import RideOnTheWay from './screens/RideOnTheWay';
import RideArrived from './screens/RideArrived';
import SendMessage from './screens/SendMessage';
import SearchLocation from './screens/SearchLocation';

const Stack = createStackNavigator();

const Navigator = () => {
  return (
    <Stack.Navigator headerMode='none'>
      <Stack.Screen
        name="Starting" 
        component={Starting}
      />
      <Stack.Screen 
        name="Destination" 
        component={Destination}
      />
      <Stack.Screen 
        name="CarSelection"
        component={CarSelection}
      />
      <Stack.Screen 
        name="WaitingForCar"
        component={WaitingForCar}
      />
      <Stack.Screen 
        name="RideOnTheWay"
        component={RideOnTheWay}
      />
      <Stack.Screen 
        name="RideArrived"
        component={RideArrived}
      />
      <Stack.Screen 
        name="SendMessage"
        component={SendMessage}
      />
      <Stack.Screen 
        name="SearchLocation"
        component={SearchLocation}
      />
    </Stack.Navigator>
  )
}

export default Navigator;