import React from 'react';
import { 
  TouchableWithoutFeedback,
  View,
  Text,
  StyleSheet,
 } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

export const IconedButton = ({text, style, onPress, iconName, onLongPress}) => {
  return (
    <TouchableWithoutFeedback onPress={() => onPress()} onLongPress={() => onLongPress()}>
      <View style={[styles.iconedButtonContainer, style]}>
        <Icon name={iconName} size={22} color="#000" />
        <Text style={styles.text}>{text}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export const ColoredButton = ({text, style, onPress, onLongPress}) => {
  return (
    <TouchableWithoutFeedback onPress={() => onPress()} onLongPress={() => onLongPress()}>
      <View style={[styles.coloredButtonContainer, style]}>
        <Text style={styles.text}>{text}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  iconedButtonContainer: {
    backgroundColor: '#F0F0F0',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
  },
  coloredButtonContainer: {
    // width: '90%',
    backgroundColor: '#FFD825',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  }, 
  text: {
    fontSize: 16,
    marginLeft: 10
  }
})