import React from 'react';
import {
  View,
  StyleSheet,
  TextInput
} from 'react-native';

export const InputBox = ({ placeholder, boxStyle, containerStyle, editable, value, onChangeText, onFocus }) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <TextInput 
        style={[styles.input, boxStyle]}
        onChangeText={(text) => onChangeText(text)}
        placeholder={placeholder}
        underlineColorAndroid="transparent"
        editable={editable}
        onFocus={() => onFocus()}
        value={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 40,
    backgroundColor: '#f1f1f1',
  },
  input: {
    flex: 1,
    fontSize: 16,
    marginHorizontal: 16
  }
})