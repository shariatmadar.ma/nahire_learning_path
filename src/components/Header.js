import React from 'react';
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

export const Header = ({style, back, onBackPress, menu, onMenuPress}) => {
  return (
    <View style={[styles.container, style]}>
      {back ?
        <TouchableWithoutFeedback onPress={() => onBackPress()}>
          <Icon name="md-arrow-back" size={30} color="#000"/>
        </TouchableWithoutFeedback>
        :
        null
      }
      {menu ?
        back ?
          <TouchableWithoutFeedback onPress={() => onMenuPress()}>
            <Icon name="md-menu" size={30} color="#000"/>
          </TouchableWithoutFeedback>
          :
          <TouchableWithoutFeedback onPress={() => onMenuPress()}>
            <View style={{width: '100%', alignItems: 'flex-end'}}>
              <Icon name="md-menu" size={30} color="#000"/>
            </View>
          </TouchableWithoutFeedback>
        :
        null
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    marginHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
})