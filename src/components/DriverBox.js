import React, { useState } from 'react';
import { 
  View, 
  Image,
  Text,
  TouchableWithoutFeedback,
  Linking,
  StyleSheet 
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import {IconedButton, ColoredButton} from '../components';

export const DriverBox = ({
  style,
  driverImage,
  driverName,
  driverRating,
  carNumber,
  carName,
  carColor,
  tripPrice,
  carSize,
  phoneNumber,
  onCancelPress,
  onMessagePress,
}) => {

  const [open, setOpen] = useState(false);

  return (
    <View style={[styles.container, style, {height: open ? 300 : 90, flexDirection: !open ? 'row' : 'column', justifyContent: open? null : 'space-between'}]}>
      {!open?
        <>
          <View style={styles.arrow}>
            <TouchableWithoutFeedback onPress={() => setOpen(true)}>
                <Icon name={"angle-up"} size={30} color="#000" />
            </TouchableWithoutFeedback>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Image 
              source={driverImage}
              style={styles.avatar}
            />
            <View>
              <Text style={styles.driverName}>{driverName}</Text>
              <Text style={styles.carName}>{carName} - {carColor}</Text>
            </View>
          </View>
          <View style={styles.numberContainer}>
            <Text style={styles.number}>{carNumber}</Text>
          </View>
        </>
        :
        <>
          <View style={styles.arrow}>
            <TouchableWithoutFeedback onPress={() => setOpen(false)}>
                <Icon name={"angle-down"} size={30} color="#000" />
            </TouchableWithoutFeedback>
          </View>
          <View style={styles.imageAndNameAndNumberContainer}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image 
                source={driverImage}
                style={styles.avatar}
              />
              <Text style={styles.openDriverName}>{driverName}</Text>
            </View>
            <View style={styles.numberContainer}>
              <Text style={styles.number}>{carNumber}</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', alignItems:'center'}}>
            <Icon name="star" size={14} color="#000"/>
            <Text style={{fontSize: 14, marginLeft: 3}}>{driverRating}</Text>
          </View>
          <View style={styles.detailsContainer}>
            <View>
              <Text style={styles.openCarName}>{carName} - {carColor}</Text>
              <Text style={styles.tripPrice}>${tripPrice}CAD</Text>
            </View>
          </View>
          <TouchableWithoutFeedback onPress={() => onMessagePress()}>
            <View style={styles.messageContainer}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon name="comment" size={20} color="#000" />
                <Text style={styles.messageButton}>send a message to driver</Text>
              </View>
              <Icon name="chevron-right" size={15} color="#000" />
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.btnContainer}>
            <IconedButton 
              iconName="phone"
              onPress={() => Linking.openURL(`tel:${phoneNumber}`)}
              text="call driver"
              style={[styles.btn, {backgroundColor: '#FFD825'}]}
            />
            <ColoredButton 
              onPress={() => null}
              text="cancel ride"
              style={[styles.btn, {backgroundColor: '#F1F1F1'}]}
            />
          </View>
        </>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#FFF',
    paddingTop: 25,
    paddingBottom: 13,
    paddingHorizontal: 25
  },
  arrow: {
    position: 'absolute',
    right: 25,
    top: 5
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    resizeMode: 'contain',
    marginRight: 10,
  },
  driverName: {
    fontSize: 12,
    fontWeight: 'bold',
    marginBottom: 6
  },
  carName: {
    fontSize: 12,
    width: '90%',
  },
  numberContainer: {
    borderWidth: 1,
    borderColor: '#004CA7',
    paddingHorizontal: 5,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 5
    // marginTop: 7,
    // marginBottom: 3
  },
  number: {
    fontSize: 23,
    color: '#004CA7'
  },
  imageAndNameAndNumberContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  openDriverName: {
    fontSize: 14
  },
  detailsContainer: {
    marginTop: 6,
    flexDirection: 'row'
  },
  openCarName: {
    fontSize: 14,
  },
  tripPrice: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 6
  },
  messageContainer: {
    flexDirection: 'row',
    marginTop: 24,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  messageButton: {
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 10
  },
  btnContainer: {
    marginTop: 24,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  btn: {
    width: '47%',
  },
})