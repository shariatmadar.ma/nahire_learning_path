import React from 'react';
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

// import Svg from 'react-native-svg';

import LocationLogo from '../assets/svg/LocationLogo.svg';

export const LocationButton = ({onLocationPress, style}) => {
  return (
    <TouchableWithoutFeedback>
      <View style={[styles.container, style]}>
        <LocationLogo width="100%" height="100%" />
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF'
  }
})
