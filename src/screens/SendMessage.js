import React, { useState, useEffect } from 'react';
import { 
  View,
  StatusBar,
  Text,
  TextInput,
  Dimensions,
  ToastAndroid,
  PermissionsAndroid,
  StyleSheet 
} from 'react-native';

import {Header, ColoredButton} from '../components';
import { CheckAndroidPermission, GetAndroidPermission, sms } from '../utils';

const { width, height } = Dimensions.get('window');

const SendMessage = ({route, navigation}) => {

  const [message, setMessage] = useState("");
  const [permission, setPermission] = useState(false);
  const {number} = route.params;

  const GetSmsPermission = () => {
    CheckAndroidPermission(PermissionsAndroid.PERMISSIONS.SEND_SMS).then((data) =>{
      console.log('permission granted');
      setPermission(true);
    })
    .catch(async (error) => {
      await GetAndroidPermission(PermissionsAndroid.PERMISSIONS.SEND_SMS).then((data) => {
        console.log('permission granted');
        setPermission(true);
      })
      .catch((error) => {
        setPermission(false);
      })
    })
  }

  const sendSms = () => {
    if (!permission) {
      ToastAndroid.showWithGravityAndOffset(
        'Permission not granted',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        0,
        120
      );
      GetSmsPermission();
    } else {
      sms.send(number, message);

    }
  }

  useEffect(() => {
    GetSmsPermission();
    console.log(number);
  },[])

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" barStyle={"dark-content"} />
      <Header back onBackPress={() => navigation.pop()} />
      <Text style={styles.pageTitle}>send a message to driver</Text>
      <ColoredButton 
        text="I'm coming"
        style={styles.samples}
        onPress={() => setMessage("I'm coming")}
      />
      <ColoredButton 
        text="I'll be there in a minute"
        style={styles.samples}
        onPress={() => setMessage("I'll be there in a minute")}
      />
      <ColoredButton 
        text="Lorem ipsum dolor sit"
        style={styles.samples}
        onPress={() => setMessage("Lorem ipsum dolor sit")}
      />
      <ColoredButton 
        text="Lorem ipsum dolor sit"
        style={styles.samples}
        onPress={() => setMessage("Lorem ipsum dolor sit")}
      />
      <ColoredButton 
        text="Lorem ipsum dolor sit"
        style={styles.samples}
        onPress={() => setMessage("Lorem ipsum dolor sit")}
      />
      <View style={styles.inputContainer}>
        <TextInput
          // ref = {(input) => { _BODY = input; }}
          style={styles.input}
          placeholder="write your message here" 
          multiline={true}
          underlineColorAndroid="transparent"
          maxLength={100}
          value={message}
          onChangeText={(text) => setMessage(text)}
        />
      </View>
      <ColoredButton 
        text="send"
        style={styles.sendBtn}
        onPress={() => sendSms()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  pageTitle: {
    fontSize: 26,
    marginHorizontal: 24,
    marginBottom: 24,
    marginTop: 6
  },
  samples: {
    backgroundColor: '#F1F1F1',
    width: width - 48,
    height: 44,
    marginVertical: 8
  },
  inputContainer: {
    width: width - 48,
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 16, 
    height: 100,
    borderColor: '#EEE',
    paddingHorizontal: 8,
    borderWidth: 1,
  },
  input: {
    flex: 1,
    color: '#000',
    fontSize: 16,
  },
  sendBtn: {
    width: width - 48,
  }
})

export default SendMessage;