import React, { useEffect } from 'react';
import { 
  View,
  StatusBar,
  Text,
  TouchableWithoutFeedback,
  Dimensions,
  BackHandler,
  Linking,
  StyleSheet 
} from 'react-native';

import {default as Ionicon} from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

import { Header, IconedButton, ColoredButton } from '../components';

const { width, height } = Dimensions.get('window');

const RideArrived = ({navigation}) => {

  const driverNumber = "09038453075";

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    })
  },[])

  return (
    <View>
      <StatusBar translucent backgroundColor="transparent" barStyle={"dark-content"} />
      <Header menu />
      <View style={styles.titleContainer}>
        <Ionicon name="md-information-circle-outline" size={70} color="#000" />
        <Text style={styles.pageTitle}>your ride has arrived</Text>
      </View>
      <View style={styles.mapContainer}>
        <MapView 
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          showsUserLocation={true}
          showsMyLocationButton={false}
        />
      </View>
      <View style={styles.driverBox}>
        <TouchableWithoutFeedback onPress={() => navigation.navigate('SendMessage', {number: driverNumber})}>
          <View style={styles.messageContainer}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon name="comment" size={20} color="#000" />
              <Text style={styles.messageButton}>send a message to driver</Text>
            </View>
            <Icon name="chevron-right" size={15} color="#000" />
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.btnContainer}>
          <IconedButton 
            iconName="phone"
            onPress={() => Linking.openURL(`tel:${"09038453075"}`)}
            text="call driver"
            style={[styles.btn, {backgroundColor: '#FFD825'}]}
          />
          <ColoredButton 
            onPress={() => null}
            text="cancel ride"
            style={[styles.btn, {backgroundColor: '#F1F1F1'}]}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 24,
    marginTop: 8,
    marginBottom: 24
  },
  pageTitle: {
    fontSize: 26,
    marginLeft: 22,
    // backgroundColor: 'red',
    width: '50%'
  },
  mapContainer: {
    width: '100%',
    height: height - 150,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  driverBox: {
    backgroundColor: '#FFF',
    height: 160,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingHorizontal: 24
  },
  messageContainer: {
    flexDirection: 'row',
    marginTop: 24,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  messageButton: {
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 10
  },
  btnContainer: {
    marginTop: 24,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  btn: {
    width: '47%',
  },
})

export default RideArrived;