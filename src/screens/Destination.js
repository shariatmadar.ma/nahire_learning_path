import React, { useEffect, useState } from 'react';
import {
  View,
  StatusBar,
  Text,
  Dimensions,
  StyleSheet
} from 'react-native';

import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

import { ColoredButton, InputBox, LocationButton, Header } from '../components';

const { width, height } = Dimensions.get('window');

const Destination = ({route, navigation}) => {

  const [startingName, setStartingName] = useState('');
  const [startingId, setStartingId] = useState('');
  const [startingLat, setStartingLat] = useState('');
  const [startingLong, setStartingLong] = useState('');
  const [addressId, setAddressId] = useState('');
  const [addressName, setAddressName] = useState('');

  useEffect(() => {
    if (route.params.origin == 'starting') {
      console.log('starting');
      setStartingId(route.params.startingId);
      setStartingName(route.params.startingName);
      setStartingLat(route.params.startingLat);
      setStartingLong(route.params.startingLong);
    }
    if (route.params.origin == 'destination') {
      console.log('destination');
      setAddressId(addressId);
      setAddressName(addressName)
    }
  }, [route.params])

  const onSubmitDestination = () => {
    const trip = {
      originId: startingId,
      originName: startingName,
      destinationName: addressName,
      destinationId: addressId
    }
    if (addressId) {
      navigation.navigate('CarSelection', {trip});
    }
  }

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" barStyle={"dark-content"} />
      <Header back onBackPress={() => navigation.pop()}/>
      <InputBox 
        placeholder="where are you?"
        containerStyle={styles.searchBox}
        value={startingName}
        editable={false}
      />
      <InputBox 
        placeholder="where to?"
        containerStyle={styles.searchBox}
        editable={true}
        value={addressName}
        onFocus={() => navigation.navigate('SearchLocation', {origin: 'destination'})}
      />
      <View style={styles.favouritePlaces}>
        <Text style={styles.placesTitle}>favourite places:</Text>
      </View>
      <View style={styles.mapContainer}>
        <MapView 
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          showsUserLocation={true}
          showsMyLocationButton={false}
          initialRegion={{
            latitude: startingLat,
            longitude: startingLong,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
          }}
        >
          <Marker 
            coordinate={{
              latitude: startingLat,
              longitude: startingLong
            }}
          />
        </MapView>
      </View>
      <LocationButton style={styles.locationButton} />
      <ColoredButton 
        text="confirm destination"
        style={styles.button}
        onPress={() => onSubmitDestination()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  searchBox: {
    marginHorizontal: 24,
    marginTop: 16
  },
  favouritePlaces: {
    flexDirection: 'row',
    margin: 24
  },
  placesTitle:{ 
    fontSize: 16
  },
  mapContainer: {
    width: '100%',
    height: height - 187, //-212
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  button: {
    position: 'absolute',
    width: width - 48,
    bottom: 24,
  },
  locationButton: {
    position: 'absolute',
    bottom: 94,
    right: 16
  },
})

export default Destination;