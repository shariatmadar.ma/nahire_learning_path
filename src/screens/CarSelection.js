import React from 'react';
import { 
  View,
  StatusBar,
  Text,
  Dimensions,
  StyleSheet
} from 'react-native';

import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

import {Header, IconedButton, ColoredButton} from '../components';

const { width,height } = Dimensions.get('window');

const CarSelection = ({navigation}) => {

  const onSubmit = () => {
    navigation.navigate('WaitingForCar');
  }

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" barStyle={"dark-content"} />
      <Header back menu onBackPress={() => navigation.pop()}/>
      <Text style={styles.pageTitle}>select your car size</Text>
      <View style={styles.mapContainer}>
        <MapView 
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          showsUserLocation={true}
          showsMyLocationButton={false}
        />
      </View>
      <View style={styles.btnContainer}>
        <IconedButton 
          iconName="percent"
          onPress={() => null}
          text="discount"
          style={styles.btn}
        />
        <IconedButton 
          iconName="cog"
          onPress={() => null}
          text="ride options"
          style={styles.btn}
        />
      </View>
      <ColoredButton 
        text="confirm"
        style={styles.button}
        onPress={() => onSubmit()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  pageTitle: {
    fontSize: 26,
    marginTop: 16,
    marginLeft: 24,
    marginBottom: 24
  },
  mapContainer: {
    width: '100%',
    height: '30%',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  btnContainer: {
    marginTop: 16,
    marginHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  btn: {
    width: '47%',
  },
  button: {
    width: width - 48,
    marginTop: 16
  },
})

export default CarSelection;