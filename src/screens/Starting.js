import React, { useEffect, useRef, useState } from 'react';
import {
  View,
  StatusBar,
  ToastAndroid,
  Dimensions,
  StyleSheet,
} from 'react-native'

import Icon from 'react-native-vector-icons/Ionicons';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import { useLazyQuery } from '@apollo/react-hooks';

import { ColoredButton, InputBox, LocationButton } from '../components';
import { getAddressDetails, getAddressFromLatLong } from '../api';

const { width, height } = Dimensions.get('window');

const Starting = ({route, navigation}) => {
  
  let latState, longState;
  
  const map = useRef();
  const marker = useRef();

  const [getAdressDetailsQuery, {loading, data, error}] = useLazyQuery(getAddressDetails);
  // const [getAddressFromLatLongQuery, {loading, data, error}] = useLazyQuery(getAddressFromLatLong);

  const {addressId , addressName} = route.params ? route.params: {};

  useEffect(() => {
    console.log(route, 'route');
    console.log(addressId, 'addressId');
    getAdressDetailsQuery({variables: {id: addressId}});
  }, [addressId]);

  if(loading) console.log("loading");
  if(error) console.log("ridi", error);
  
  if(data) {
    const {lat,long} = data.getAddressDetails.location;
    latState = lat;
    longState = long;
    const region = {
      latitude: lat,
      longitude: long,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01,
    }
    const location = {
      latitude: lat,
      longitude: long,
    }
    map.current.animateToRegion(region);
    marker.current.animateMarkerToCoordinate(location);
  }

  const handleLocationPress = (coordinate) => {
    const {latitude, longitude} = coordinate;
    marker.current.animateMarkerToCoordinate(location);
  }

  const onSubmitStartingPoint = () => {
    if (addressId) {
      navigation.navigate('Destination', {
        origin: 'starting',
        startingName: addressName,
        startingId: addressId,
        startingLat: latState,
        startingLong: longState,
      })
    } 
    else {
      ToastAndroid.showWithGravityAndOffset(
        'Starting point must be defined',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        0,
        120
      );
    }
  }

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" barStyle={"dark-content"} />
      <HomeHeader />
      <InputBox 
        placeholder="where are you?"
        value={addressName}
        containerStyle={styles.searchBox}
        onFocus={() => navigation.navigate('SearchLocation', {origin: 'starting'})}
      />
      <View style={styles.mapContainer}>
        <MapView
          ref={map}
          onPress={(event) => handleLocationPress(event.nativeEvent.coordinate)} 
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          showsUserLocation={true}
          showsMyLocationButton={false}
        >
          <Marker
            ref={marker}
            coordinate={{
              latitude: 0,
              longitude: 0
            }}
          />
        </MapView>
      </View>
      <LocationButton style={styles.locationButton} />
      <ColoredButton 
        text="confirm starting location"
        style={styles.button}
        onPress={() => onSubmitStartingPoint()}
      />
    </View> 
  );
};

const HomeHeader = () => {
  return (
    <View style={styles.headerContainer}>
      <Icon name="md-home" color="#000" size={30} />
      <Icon name="md-menu" color="#000" size={30} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  searchBox: {
    marginHorizontal: 24,
    marginTop: 16
  },
  mapContainer: {
    width: '100%',
    height: height - 120, //-146
    marginTop: 20,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  button: {
    position: 'absolute',
    width: width - 48,
    bottom: 24,
  },
  locationButton: {
    position: 'absolute',
    bottom: 94,
    right: 16
  },
  headerContainer: {
    marginTop: 40,
    marginHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
})

export default Starting;