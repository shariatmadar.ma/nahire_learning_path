import React, { useEffect } from 'react';
import { 
  View,
  Text,
  StatusBar,
  Dimensions,
  BackHandler,
  StyleSheet
} from 'react-native';

import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

import { DriverBox, Header } from '../components';

const { width, height } = Dimensions.get('window');

const RideOnTheWay = ({navigation}) => {

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    })
  },[])

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" barStyle={"dark-content"} />
      <Header menu onMenuPress={() => navigation.navigate('RideArrived')} />
      <Text style={styles.pageTitle}>your ride is on the way!</Text>
      <View style={styles.mapContainer}>
        <MapView 
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          showsUserLocation={true}
          showsMyLocationButton={false}
        />
      </View>
      <DriverBox 
        style={styles.driverBox}
        driverImage={require('../assets/img/car.png')}
        driverName="Liam Parker"
        carName="Ford Fiesta 2019"
        carColor="dark brown"
        carNumber="BYTR741"
        driverRating="4.8"
        tripPrice="103.99"
        phoneNumber="09360902255"
        onMessagePress={() => navigation.navigate('SendMessage', {number: "09038453075"})}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  pageTitle: {
    fontSize: 26,
    marginLeft: 24,
    marginBottom: 24
  },
  mapContainer: {
    width: '100%',
    height: height - 100,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  driverBox: {
    position: 'absolute',
    bottom: 0
  }
})

export default RideOnTheWay;