import React, { useEffect } from 'react';
import {
  View, 
  Text,
  StatusBar,
  Image,
  Dimensions,
  BackHandler,
  StyleSheet
} from 'react-native';
import { ColoredButton } from '../components';

const { width,height } = Dimensions.get('window');

const WaitingForCar = ({navigation}) => {

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    })
  },[])

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" barStyle={"dark-content"} />
      <Text style={styles.pageTitle}>finding a ride for you…</Text>
      <Image 
        source={require('../assets/img/car.png')}
        style={styles.image}
      />
      <ColoredButton 
        text="hold to cancel"
        style={styles.button}
        onLongPress={() => onCancelPress()}
        onPress={() => navigation.navigate('RideOnTheWay')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  pageTitle: {
    fontSize: 26,
    marginTop: 84,
    marginLeft: 24,
  },
  image: {
    height: height / 4,
    marginTop: 100,
    resizeMode: 'contain',
    alignSelf: 'center'
  },
  button: {
    width: width - 48,
    position: 'absolute',
    bottom: 16,
    backgroundColor: '#F1F1F1'
  }
})

export default WaitingForCar;