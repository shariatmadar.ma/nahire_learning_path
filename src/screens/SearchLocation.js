import React, { useState, useEffect } from 'react';
import { 
  View,
  StatusBar,
  Text,
  TextInput,
  FlatList,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Image,
  StyleSheet,
 } from 'react-native';

import Voice from 'react-native-voice';
import Icon from 'react-native-vector-icons/FontAwesome';

import { useLazyQuery } from '@apollo/react-hooks';

import { InputBox, Header } from '../components';
import {getAddressSuggestions} from '../api';

const SearchLocation = ({route, navigation}) => {
  
  const [getAddress, { loading, error, data }] = useLazyQuery(getAddressSuggestions);

  const [recognized, setRecognized] = useState('');
  const [started, setStarted] = useState('');
  const [result, setResult] = useState([]);

  const {origin} = route.params;

  const _startRecognition = async (e) => {
    setRecognized('');
    setStarted('');
    setResult([]);
    try {
      await Voice.start('en-US');
    } catch (e) {
      console.error(e);
    }
  }

  Voice.onSpeechStart = (e) => {
    setStarted('√');
  }

  Voice.onSpeechRecognized = (e) => {
    setRecognized('√');
  }

  Voice.onSpeechResults = (e) => {
    console.log(e)
    setResult(e.value[0]);
    getAddress({variables: {query: e.value[0]}});
  }

  const handleChange = (text) => {
    setResult(text);
    getAddress({variables: {query: text}});
  }

  const handleNavigation = (item) => {
    // console.log(origin);
    switch (origin) {
      case 'starting':
        // console.log(item);
        navigation.navigate('Starting', {
          addressId: item.id,
          addressName: item.address,
        });
        break;
      case 'destination': 
        navigation.navigate('Destination', {
          origin: 'destination',
          addressId: item.id,
          addressName: item.address,
        });
        break;
    }
  }

  // useEffect(() => {
  //   console.log(route.params.origin);
  // },[])

  const _renderItem = (item) => {
    return (
      <TouchableWithoutFeedback onPress={() => handleNavigation(item)}>
        <View style={styles.listItem}>
          <Icon name="map-marker" size={20} color="#000"/>
          <Text style={styles.itemAddress}>
            {item.address.length > 42 ?
              item.address.slice(0,42) + "..."
            :
              item.address
            }
          </Text>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  // if(data) console.log(data);

  return(
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" barStyle={"dark-content"} />
      <Header back onBackPress={() => navigation.pop()} />
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          onChangeText={(text) => handleChange(text)}
          placeholder={"enter an address"}
          autoFocus={true}
          underlineColorAndroid="transparent"
          value={result}
        />
        <TouchableWithoutFeedback onPress={() => _startRecognition()}>
          <View style={styles.voiceButtonContainer}>
            <Icon name="microphone" size={25} color="#000" />
          </View>
        </TouchableWithoutFeedback>
      </View>
      {loading ?
        <View style={styles.indicator}>
          <ActivityIndicator />
        </View>
      :
      data && data.guessAddress.length !== 0 ?
        <View style={styles.listContainer}>
          <FlatList 
            data={data.guessAddress}
            renderItem={({item}) => _renderItem(item)}
            keyExtractor={item => item.id}
          />
        </View>
      : 
      data && data.guessAddress.length === 0 ?
        <View>
          <Image style={styles.notFoundImage} source={require('../assets/img/not_found.png')} />
          <Text style={styles.notFoundText}>no result found</Text>
        </View>
      :
        null
      }
    </View>
  );
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  inputContainer: {
    height: 40,
    marginHorizontal: 24,
    marginTop: 16,
    backgroundColor: '#f1f1f1',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderRadius: 5
  },
  input: {
    flex: 1,
    fontSize: 16,
    marginRight: 8
  },
  voiceButtonContainer: {
    width: 40, 
    height: '100%', 
    justifyContent: 'center', 
    alignItems:'center'
  },
  indicator: {
    marginTop: 24
  },
  listContainer: {
    margin: 24,
  },
  listItem: {
    flexDirection: 'row',
    marginVertical: 12
  },
  itemAddress: {
    marginLeft: 10
  },
  notFoundImage: {
    height: '30%',
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: '30%'
  },
  notFoundText: {
    textAlign: 'center',
    alignSelf: 'center',
    marginTop: 16,
    fontSize: 16
  }
})

export default SearchLocation;