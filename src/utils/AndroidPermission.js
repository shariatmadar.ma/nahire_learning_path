import { PermissionsAndroid, Platform } from 'react-native';

export const CheckAndroidPermission = () => {
  return new Promise(async (resolve, reject) => {
      if (Platform.OS === 'android') {
          const result = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
          if (result) resolve(true);
          else reject(false);
      } else {
          resolve(true);
      }
  });
};

export const GetAndroidPermission = (permission) => {
  return new Promise(async(resolve, reject) => {
      if (Platform.OS === 'android') {
          const permissions = await PermissionsAndroid.request(permission);
          if (permissions === PermissionsAndroid.RESULTS.GRANTED) resolve(true);
          else reject(false);
      } else {
          resolve(true);
      }
  });
};