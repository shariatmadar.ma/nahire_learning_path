import { ToastAndroid } from 'react-native';
import SmsAndroid from 'react-native-get-sms-android';

class Sms {
  
  send(number, body) {
    SmsAndroid.autoSend(
      number,
      body,
      (fail) => {
        ToastAndroid.showWithGravityAndOffset(
          'Message not sent',
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
          0,
          120
        );
      },
      (success) => {
        ToastAndroid.showWithGravityAndOffset(
          'Message sent succesfully',
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
          0,
          120
        );
      }
    )
  }

}

export const sms = new Sms();