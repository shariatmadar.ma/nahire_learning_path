import { gql } from 'apollo-boost';

export const getAddressSuggestions = gql`
query($query: String!) {
  guessAddress(query: $query) {
    address
    id
  }
}
`; 

export const getAddressDetails = gql`
  query($id: String!) {
    getAddressDetails(addressId: $id) {
      id
      address
      location {
        long
        lat
      }
    }
  }
`;

export const getAddressFromLatLong = gql`
  query getaddress($location: LocationInput!) {
    getAddressFromLongLat(location: $location)
  }
`;